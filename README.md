# Geotuileur - QGIS Plugin

[![pipeline status](https://gitlab.com/Oslandia/qgis/ign-geotuileur/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/ign-geotuileur/-/commits/master)

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)  
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Oslandia_ign-geotuileur&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=Oslandia_ign-geotuileur)

## Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages:  <https://oslandia.gitlab.io/qgis/ign-geotuileur/>.

----

## License

The plugin is distributed under the terms of the [`GPLv2+` license](LICENSE).  
Le plugin est distribué sous licence [`GPLv2+` license](LICENSE).
