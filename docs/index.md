# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

{{ title }} est un plugin pour QGIS 3.22+ qui a pour objectif de permettre de reproduire le parcours utilisateur du site Géotuileur dont voici 2 vidéos le détaillant :

<iframe src="https://player.vimeo.com/video/722998381?h=3dcf3a2ef0&amp;title=0&amp;byline=0&amp;portrait=0&amp;speed=0&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="100%" height="450" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="screen-recording.webm"></iframe>

----

```{toctree}
---
caption: Usage
maxdepth: 1
---
Installation <usage/installation>
```

----

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
Wiki (fr) <https://gitlab.com/Oslandia/qgis/ign-geotuileur/-/wikis/home>
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
```
